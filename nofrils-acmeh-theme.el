;;; nofrils-acme-theme.el --- A monochromatic, purple-gray theme

;; Copyright (C) 2020 Brihadeesh S (@gitlab/peregrinator; @github/peregrinat0r)
;;
;; Permission is hereby granted, free of charge, to any person obtaining a copy
;; of this software and associated documentation files (the "Software"), to deal
;; in the Software without restriction, including without limitation the rights
;; to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
;; copies of the Software, and to permit persons to whom the Software is
;; furnished to do so, subject to the following conditions:

;; The above copyright notice and this permission notice shall be included in all
;; copies or substantial portions of the Software.

;; THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
;; IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
;; FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
;; AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
;; LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
;; OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
;; SOFTWARE.

;; Author: Brihadeesh S <brihadeesh@protonmail.com>
;; URL: https://gitlab.com/peregrinator/nofrils.el
;; Version: 0.2
;; Package-Requires: ((colourless-themes "0.1"))
;; License: MIT
;; Keywords: faces theme

;;; Commentary:
;; This is a port of nofrils themes, nearly colourless vim themes by Robert Melton
;; https://github.com/robertmeta/nofrils;
;; Made with the `colorless-themes` macro from Thomas Letan
;; https://git.sr.ht/~lthms/colorless-themes.el

;;; Code:
(require 'colourless-themes)

(deftheme nofrils-acme "A near monochromatic acme-esque theme")

(colourless-themes-make nofrils-acme
                       "#ffffd7"    ; bg
                       "#ffffd7"    ; bg+
                       "#ffff87"    ; current-line
                       "#d7afff"    ; fade
                       "#000000"    ; fg
                       "#00ffff"    ; fg+
                       "#af8700"    ; primary
                       "#ff5555"    ; red
                       "#ff5555"    ; orange
                       "#875f00"    ; yellow
                       "#008000")   ; green

;;;###autoload
(when (and (boundp 'custom-theme-load-path) load-file-name)
  (add-to-list 'custom-theme-load-path
               (file-name-as-directory (file-name-directory load-file-name))))

(provide-theme 'nofrils-acme)
(provide 'nofrils-acme-theme)
;;; nofrils-acme-theme.el ends here
