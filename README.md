# nofrils colourschemes

*This is work in progress*

An Emacs port of Robert Melton's vim themes
[nofrils](https://github.com/robertmeta/nofrils). This now uses my fork of
the [`colourless-themes`](https://github.com/peregrinat0r/colourless-themes.el) 
macro and it will have to be installed separately for this to work properly.

## Screenshot(s)

> (coming soon)

## Terminal colours (WIP)
 
> (coming soon)
